import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container h-100">
        <div className="row h-100 justify-content-center align-items-center top">
          <Link to="/list" className="btn btn-outline-primary">List Characters</Link>
        </div>
      </div>
    )
  }
}

export default App;
