import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'

class View extends Component {
    constructor(props) {
        super(props)
        this.state = {
            character : {}
        }
        
    }

    componentDidMount() {
        if(!this.props.location["param"]) {
            this.props.history.push('/list')
            return <Redirect to="/list"/>
        }
        fetch('http://localhost:3000/api/characters/' + this.props.location.param.id)
        .then(response => response.json())
        .then(result => {
            this.setState({ character: result })
        })
    }
    render() {
        

        return (
            <div className="container">
                <Link className="float-left top-10 badge badge-primary cursor" to="/list">Back</Link><h3 className="top-10 float-left">character</h3>
                
                <table className="table table-bordered table-sm">
                    <tbody>
                        <tr>
                            <td rowSpan='5'><img alt="Game Of Thrones" src={this.state.character.image}/></td>
                            <th scope="row">Name</th>
                            <td>{ this.state.character.name }</td>
                        </tr>
                        <tr>
                            <th scope="row">Gender</th>
                            <td>{ this.state.character.gender }</td>
                        </tr>
                        <tr>
                            <th scope="row">Slug</th>
                            <td>{ this.state.character.slug }</td>
                        </tr>
                        <tr>
                            <th scope="row">House</th>
                            <td>{ this.state.character.house }</td>
                        </tr>
                        <tr>
                            <th scope="row">Titles</th>
                            <td>{ this.state.character.titles }</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default View