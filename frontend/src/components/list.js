import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class List extends Component {
    constructor() {
        super()
        this.state = {
            title : 'Game Of Thrones',
            characters : [],
            query : '',
            currentPage : 1,
            todosPerPage : 10,
            default : []
        }

        this.handlerClick = this.handlerClick.bind(this);
    }

    handlerInputChange = (event) => {
        this.setState({ query: event.target.value })
        
        function Count(entry) {
            return event.target.value.length > 0 && entry.name.toLowerCase().indexOf(event.target.value) !== -1
        }

        this.setState({ characters: (this.state.default.filter(Count).length === 0) ? this.state.default : this.state.default.filter(Count) })
        //console.log(this.state.default.filter(Count));
        
    }

    handlerClick(event) {
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    componentDidMount() {
        fetch('http://localhost:3000/api/characters')
        .then(response => response.json())
        .then(result => {
            this.setState({ characters: result })
            this.setState({ default: result })
        })
    }
    render() {
        const { characters, currentPage, todosPerPage } = this.state;

        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = characters.slice(indexOfFirstTodo, indexOfLastTodo);

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(characters.length / todosPerPage); i++) {
          pageNumbers.push(i);
        }
        const pjs = currentTodos.map((entry, i) => {
            if(this.state.query !== "" && entry.name.toLowerCase().indexOf(this.state.query) === -1) {
                return null
            }

            return (
                <tr key={i}>
                    <td>{ entry.name }</td>
                    <td>{ entry.house }</td>
                    <td>{ entry.gender }</td>
                    <td className="text-center">
                    <Link to={{pathname: '/view', param : { id: entry._id }}} className="badge badge-primary cursor">+</Link>
                    </td>
                </tr>
            )
        })

        

        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <li className="page-item page-link" key={number} id={number} onClick={this.handlerClick}>
                    {number}
                </li>
            );
        });

        

        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 top-10">
                        <h3 className="float-left">{ this.state.title }</h3>
                        <input onChange={this.handlerInputChange} className="float-right" type="text" placeholder="Search"/>
                    </div>
                    
                    <table className="table table-bordered table-sm">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">House</th>
                                <th scope="col">Gender</th>
                                <th className="text-center" scope="col">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            { pjs }
                        </tbody>
                    </table>
                    <ul className="pagination">
                        { renderPageNumbers }
                    </ul>
                </div>
            </div>
        )
    }
}

export default List