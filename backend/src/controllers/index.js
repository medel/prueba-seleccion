import { connect } from './../database'
import { ObjectId } from 'mongodb'

const Controllers = {
    welcomeUser : (req, res) => {
        res.send("Bienvenido a mi API")
    },

    allCharacters : async (req, res) => {
        const db = await connect()
        const result = await db.collection('characters').find({}).toArray()
        res.json(result)
    },

    findCharacter : async (req, res) => {
        const { id } = req.params
        const db = await connect()
        // ObjectId ??
        const result = await db.collection('characters').findOne({_id:id})
        res.json(result)
    }
}

export default Controllers