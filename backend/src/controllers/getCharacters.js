import axios from 'axios'
import { connect } from './../database'

export async function getCharacters() {
    const URL = 'https://api.got.show/api/show/characters'
    try {
        const db = await connect()
        const response = await axios.get(URL);
        const data = response.data;
        await db.collection('characters').drop()
        console.log('Delete Collection Characters')
        const result = await db.collection('characters').insertMany(data)
        console.log(`Inserted ${result.insertedCount} registers`);
        console.log('Load Characters successful')
    } catch (error) {
        console.log(error);
    }
}