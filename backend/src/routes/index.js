import { Router } from 'express'
const router = Router()

// Controllers
import Controllers from './../controllers/index' 

// Welcome Router
router.get('/', Controllers.welcomeUser)

// Show Characaters
router.get('/api/characters', Controllers.allCharacters)

// Get Character ID
router.get('/api/characters/:id', Controllers.findCharacter)

export default router