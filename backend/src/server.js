import express from 'express'
import bodyParser from 'body-parser'
import routes from './routes/index'
import cors from 'cors'

const app = express()

// Configuraciones
app.set('port', process.env.PORT || 3000)

// MiddleWare
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors())

// Rutas
app.use(routes)

export default app