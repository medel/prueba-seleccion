import '@babel/polyfill'
import app from './server'
import { connect } from './database'
import { getCharacters } from './controllers/getCharacters'


async function main() {
    await app.listen(app.get('port'))
    await connect()
    await getCharacters()
    console.log(`Server run on port: ${app.get('port')}`)

}

main()